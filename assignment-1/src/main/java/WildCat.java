/*
Author: Annida Safira Arief
NPM: 1706040050
Class: B
program untuk membuat wildcat
 */

public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    //constructor class wildcat
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    //menghitung BMI wildcat
    public double computeMassIndex() {
        return ((double) this.weight / (length * length * 0.01 * 0.01));
    }
}
