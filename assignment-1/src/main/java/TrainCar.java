/*
Author: Annida Safira Arief
NPM: 1706040050
Class: B
program untuk membuat gerbong berisi wildcat
 */

public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    static int catCount = 0; //variable untuk menghitung wildcat yang telah diinisiasi
    WildCat cat;
    TrainCar next;

    //constructor class TrainCar
    public TrainCar(WildCat cat) {
        this.cat = cat;
        catCount += 1;
        // TODO Complete me!
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
        catCount += 1;
    }

    //method untuk menghitung total massa kereta
    public double computeTotalWeight() {
        // TODO Complete me!
        if (next == null) {
            return (cat.weight + EMPTY_WEIGHT);
        } else {
            return (cat.weight + EMPTY_WEIGHT + next.computeTotalWeight());
        }
    }

    //method untuk menghitung BMI wildcat dalam kereta
    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (next == null) {
            return (cat.computeMassIndex());
        } else {
            return (cat.computeMassIndex() + next.computeTotalMassIndex());
        }
    }

    //method untuk mencetak kereta
    public void printCar() {
        // TODO Complete me!
        if (next == null) {
            System.out.print("--(" + cat.name + ")");
        } else {
            System.out.print("--(" + cat.name + ")");
            next.printCar();
        }
    }

    //mwthod untuk menghitung rata-rata BMI wildcat
    public double averageMassIndex() {
        if (catCount == 1) {
            return ((double) computeTotalMassIndex() / catCount);
        }
        double average = (double) computeTotalMassIndex() / (catCount - 1);
        return average;
    }

    //method untuk mengetahui index dari rata-rata BMI wildcat pada kereta
    public void massIndexType() {
        if (averageMassIndex() < 18.5) {
            System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex());
            System.out.println("In average, the cats in the train are underweight");
        } else if (averageMassIndex() < 25) {
            System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex());
            System.out.println("In average, the cats in the train are normal");
        } else if (averageMassIndex() < 30) {
            System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex());
            System.out.println("In average, the cats in the train are overweight");
        } else {
            System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex());
            System.out.println("In average, the cats in the train are obese");
        }
    }

    //method untuk memutus hubungan antara kereta yang baru diinisiasi dengan kereta yang akan berangkat
    public void nextNull() {

        next = null;
    }

    //method untuk memberangkatkan kereta
    public void isDepart() {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<");
        printCar();
        System.out.println();
        if (next != null) {
            next.massIndexType();
        } else {
            massIndexType();
        }
        catCount = 1;

    }


}