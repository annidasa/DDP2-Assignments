/*
Author: Annida Safira Arief
NPM: 1706040050
Class: B
program main dari TP 1
 */

import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //meminta jumlah kucing dari user
        int jumlahKucing = Integer.parseInt(input.nextLine());


        //meminta data dari wildcat pertama dan memasukkan ke dalam trsin csr
        String[] dataKucing = input.nextLine().split(",");
        WildCat kucing = new WildCat(dataKucing[0], Double.parseDouble(dataKucing[1]), Double.parseDouble(dataKucing[2]));
        TrainCar gerbong = new TrainCar(kucing);

        //meminta data dari wildcat selanjutnya
        if (jumlahKucing > 1) {
            for (int i = 0; i < jumlahKucing - 1; i++) {
                dataKucing = input.nextLine().split(",");
                WildCat nextKucing = new WildCat(dataKucing[0], Double.parseDouble(dataKucing[1]), Double.parseDouble(dataKucing[2]));
                TrainCar nextGerbong = new TrainCar(nextKucing, gerbong);
                if (nextGerbong.computeTotalWeight() > THRESHOLD) {
                    gerbong.isDepart();
                    kucing = nextKucing;
                    gerbong = nextGerbong;
                    nextGerbong.nextNull();
                } else {
                    kucing = nextKucing;
                    gerbong = nextGerbong;
                }

            }
        }
        if (gerbong != null) {
            gerbong.isDepart();
            gerbong.catCount=0;
        }

    }
}
