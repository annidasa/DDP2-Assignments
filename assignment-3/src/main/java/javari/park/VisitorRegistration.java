/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk registrasi visitor
 * */
package javari.park;

import java.util.ArrayList;
import java.util.List;

public class VisitorRegistration implements Registration {
    //agar id yang dihasilkan unik, dibuat suatu id generator
    private static int idCounter = 1;
    private String name;
    private int id;
    private ArrayList<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public VisitorRegistration(String name) {
        this.name = name;
        this.id = VisitorRegistration.idCounter;
        idAdder();
    }

    //method untuk menggenerate id dengan terus dijumlahkan
    private static void idAdder() {
        idCounter += 1;
    }

    @Override
    public int getRegistrationId() {
        return id;
    }

    @Override
    public String getVisitorName() {
        return name;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }

    public void addSelectedAttraction(SelectedAttraction selected) {
        selectedAttractions.add(selected);
    }
}
