/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class mendata attraction yang dipilih visitor
 * */
package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class SelectedAttractions implements SelectedAttraction {

    private List<Animal> performers = new ArrayList<>();
    protected String name;
    private String type;

    public SelectedAttractions(String name, String type, ArrayList<Animal> performers) {
        this.name = name;
        this.type = type;
        //didapat dari Animal.showableAnimal
        this.performers = performers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getPerformers() {
        return performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        try {
            this.performers.add(performer);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
