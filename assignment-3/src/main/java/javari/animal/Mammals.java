/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk hewan dengan jenis mammal
 * */
package javari.animal;


public class Mammals extends Animal {

    private boolean isPregnant;
    private static final String SECTION = "Explore the Mammals";

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, boolean isPregnant) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = isPregnant;
        if (this.isShowable()) {
            addShowable(this.getType(), this);
        }
    }

    public static String getSECTION() {
        return SECTION;
    }

    /*
     * untuk hewan jenis lion, hanya yang laki laki yang dapat melakukan atraksi, sisanya hanya sedang pregnant atau tidak
     * */
    protected boolean specificCondition() {
        if (this.getType() == "Lion") {
            return this.getGender() == Gender.MALE;
        }
        return !this.isPregnant;
    }
}