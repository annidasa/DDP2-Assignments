/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk animal dengan tipe aves
 * */
package javari.animal;

public class Aves extends Animal {
    private boolean isLayingEggs;
    public static final String SECTION = "World of Aves";

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, boolean isLayingEggs) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = isLayingEggs;
        if (this.isShowable()) {
            addShowable(this.getType(), this);
        }
    }

    public static String getSECTION() {
        return SECTION;
    }

    //specific condition yaitu tidak bisa tampil ketika sedang bertelur
    protected boolean specificCondition() {
        return !isLayingEggs;
    }
}
