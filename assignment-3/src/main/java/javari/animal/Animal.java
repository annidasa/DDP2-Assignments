package javari.animal;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represents common attributes and behaviours found in all animals
 * in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Annida Safira Arief
 * and describe the changes in the comment block
 */
public abstract class Animal {

    //hashmap untuk mendata hewan hewan yang dapat tampil sesuai tipenya
    public static HashMap<String, ArrayList<Animal>> showableAnimal = new HashMap<String, ArrayList<Animal>>();
    //hashmap untuk mendata attractions yang dilakukan tiap tipe hewan
    public static HashMap<String, ArrayList<String>> attractions = new HashMap<String, ArrayList<String>>();
    public static ArrayList<Integer> idList = new ArrayList<>();
    //arraylist of masing-masing jenis hewan
    protected static ArrayList<String> mammalType = new ArrayList<String>();
    protected static ArrayList<String> reptileType = new ArrayList<String>();
    protected static ArrayList<String> avesType = new ArrayList<String>();
    private Integer id;
    private String type;
    private String name;
    private Body body;
    private Condition condition;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Animal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.body = new Body(length, weight, gender);
        this.condition = condition;
    }

    //method getter
    public static ArrayList<String> getMammalType() {
        return mammalType;
    }

    public static ArrayList<String> getAvesType() {
        return avesType;
    }

    public static ArrayList<String> getReptileType() {
        return reptileType;
    }

    public static void addAttraction(String nama, String atraksi) {
        ArrayList<String> daftar = attractions.get(nama);
        if (daftar == null) {
            daftar = new ArrayList<String>();
            attractions.put(nama, daftar);
        }
        daftar.add(atraksi);

    }

    public static void addAttraction(String nama) {
        attractions.put(nama, null);
    }

    public static void addShowable(String key, Animal animal) {
        ArrayList<Animal> daftar = showableAnimal.get(key);
        if (daftar == null) {
            daftar = new ArrayList<Animal>();
            showableAnimal.put(key, daftar);
        }
        daftar.add(animal);
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    /**
     * Returns {@code Gender} identification of the animal.
     *
     * @return
     */
    public Gender getGender() {
        return body.getGender();
    }

    public double getLength() {
        return body.getLength();
    }

    public double getWeight() {
        return body.getWeight();
    }

    /**
     * Returns {@code Condition} of the animal.
     *
     * @return
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Determines whether the animal can perform their attraction or not.
     *
     * @return
     */
    public boolean isShowable() {
        return condition == Condition.HEALTHY && specificCondition();
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return
     */
    protected abstract boolean specificCondition();
}
