/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk animal reptile
 * */
package javari.animal;

public class Reptiles extends Animal {

    private boolean isTame;
    private static final String SECTION = "Reptillian Kingdom";

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                    double weight, Condition condition, boolean isTame) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = isTame;
        if (this.isShowable()) {
            addShowable(this.getType(), this);
        }
    }

    public static String getSECTION() {
        return SECTION;
    }

    //dapat tampil jika jinak
    protected boolean specificCondition() {
        return !isTame;
    }
}
