/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk validasi jumlah atraksi pada animal
 * */
package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AttractionReader extends CsvReader {
    ArrayList<String> attractions = new ArrayList<String>();

    public AttractionReader(Path path) throws IOException {
        super(path);
    }

    public long countValidRecords() {
        invalidLine = countInvalidRecords();
        return attractions.size();
    }

    public long countInvalidRecords() {
        long count = 0;
        for (int i = 0; i < getLines().size(); i++) {
            String[] splittedLine = getLines().get(i).split(",");
            if (splittedLine.length != 2) {
                count += 1;
            } else if (splittedLine[0].equals("")) {
                count += 1;
            } else if (!(Animal.attractions.containsKey(splittedLine[0]))) {
                count += 1;
            } else {
                Animal.addAttraction(splittedLine[0], splittedLine[1]);
                if (!(attractions.contains(splittedLine[1]))) {
                    attractions.add(splittedLine[1]);
                }
            }

        }
        return count;
    }
}