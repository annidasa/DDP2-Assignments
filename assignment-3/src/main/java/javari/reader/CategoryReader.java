/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk validasi kategori dan section
 * */

package javari.reader;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mammals;
import javari.animal.Reptiles;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class CategoryReader extends CsvReader {

    /*
     * menggunakan asumsi jumlah invalid category sama dengan section
     * */
    private ArrayList<String> category = new ArrayList<String>();

    public CategoryReader(Path path) throws IOException {
        super(path);
    }

    public long countValidRecords() {
        invalidLine = countInvalidRecords();
        return category.size();
    }

    public long countInvalidRecords() {
        long count = 0;

        for (int i = 0; i < getLines().size(); i++) {
            String[] splittedLine = getLines().get(i).split(",");
            if (splittedLine.length != 3) {
                count += 1;
            } else if (splittedLine[0].equals("")) {
                count += 1;
            } else {
                switch (splittedLine[1]) {
                    case "mammals":
                        if (!(splittedLine[2].equals(Mammals.getSECTION()))) {
                            count += 1;
                        } else {
                            if (!(Animal.getMammalType().contains(splittedLine[0]))) {
                                Animal.getMammalType().add(splittedLine[0]);
                                Animal.addAttraction(splittedLine[0]);
                                Animal.showableAnimal.put(splittedLine[0], null);
                            }
                        }
                        break;
                    case "aves":
                        if (!(splittedLine[2].equals(Aves.SECTION))) {
                            count += 1;
                        } else {
                            if (!(Animal.getAvesType().contains(splittedLine[0]))) {
                                Animal.getAvesType().add(splittedLine[0]);
                                Animal.addAttraction(splittedLine[0]);
                                Animal.showableAnimal.put(splittedLine[0], null);
                            }
                        }
                        break;
                    case "reptiles":
                        if (!(splittedLine[2].equals(Reptiles.getSECTION()))) {
                            count += 1;
                        } else {
                            if (!(Animal.getReptileType().contains(splittedLine[0]))) {
                                Animal.getReptileType().add(splittedLine[0]);
                                Animal.addAttraction(splittedLine[0]);
                                Animal.showableAnimal.put(splittedLine[0], null);

                            }
                        }
                        break;
                    default:
                        count += 1;
                        break;
                }
                if (!(category.contains(splittedLine[1]))) {
                    category.add(splittedLine[1]);
                }
            }
        }
        return count;
    }
}
