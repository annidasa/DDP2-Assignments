/*
 * Author: Annida Safira Arief
 * Kelas: DDP B
 * NPM: 1706040050
 * class untuk validasi pendataan animal
 * */
package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AnimalReader extends CsvReader {

    private ArrayList<String> validLines = new ArrayList<String>();

    public AnimalReader(Path path) throws IOException {
        super(path);
    }

    public long countValidRecords() {
        invalidLine = countInvalidRecords();
        return getLines().size() - invalidLine;
    }

    public ArrayList<String> getValidLines() {
        return validLines;
    }

    public long countInvalidRecords() {
        long count = 0;
        for (int i = 0; i < getLines().size(); i++) {
            String[] splittedLine = getLines().get(i).split(",");
            if (splittedLine.length != 8) {
                count += 1;
            } else if (Animal.idList.contains((Integer) Integer.parseInt(splittedLine[0]))) {
                count += 1;
            } else if (!(Animal.attractions.containsKey(splittedLine[1]))) {
                count += 1;
            } else if (splittedLine[2].equals("")) {
                count += 1;
            } else if (splittedLine[3].equals("")) {
                count += 1;
            } else if (splittedLine[7].equals("")) {
                count += 1;
            } else {
                try {
                    Integer.parseInt(splittedLine[0]);
                    Double.parseDouble(splittedLine[4]);
                    Double.parseDouble(splittedLine[5]);
                    validLines.add(getLines().get(i));
                } catch (Exception e) {
                    count += 1;
                }

            }
        }
        return count;
    }
}
