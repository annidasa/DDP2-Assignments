/*
 * Nama: annida safira
 * Kelas: B
 * NPM: 1706040050
 * main dari assignment 3
 * */

import javari.animal.*;
import javari.park.SelectedAttractions;
import javari.park.VisitorRegistration;
import javari.reader.AnimalReader;
import javari.reader.AttractionReader;
import javari.reader.CategoryReader;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class A3Festival {
    //arraylist dari binatang yang terdata
    private static ArrayList<Animal> animals = new ArrayList<Animal>();
    private static String path;
    private static VisitorRegistration regist = new VisitorRegistration("");
    private static boolean regName = false;

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n" +
                "\n" +
                "... Opening default section database from data. ... File not found or incorrect file!\n");
        System.out.print("Please provide the source data path: ");
        path = input.nextLine();
        //menghitung data yang valid
        CategoryReader categoryReader = new CategoryReader(Paths.get(path + "\\animals_categories.csv"));
        System.out.println("Found _" + categoryReader.countValidRecords() + "_ valid sections and _" + categoryReader.countInvalidRecords() + "_ invalid sections");
        AttractionReader attractionReader = new AttractionReader(Paths.get(path + "\\animals_attractions.csv"));
        System.out.println("Found _" + attractionReader.countValidRecords() + "_ valid attractions and _" + attractionReader.getInvalidLine() + "_ invalid attractions");
        System.out.println("Found _" + categoryReader.countValidRecords() + "_ valid animal categories and _" + categoryReader.countInvalidRecords() + "_ invalid animal categories");
        AnimalReader animalReader = new AnimalReader(Paths.get(path + "\\animals_records.csv"));
        System.out.println("Found _" + animalReader.countValidRecords() + "_ valid animal records and _" + animalReader.getInvalidLine() + "_ invalid animal records\n");

        //menginstansiasi hewan hewan yang sudah valid
        for (int i = 0; i < animalReader.getValidLines().size(); i++) {
            String[] splittedLine = animalReader.getValidLines().get(i).split(",");
            int id = Integer.parseInt(splittedLine[0]);
            if (Animal.getMammalType().contains(splittedLine[1])) {
                boolean condition = true;
                if (splittedLine[6].equals("")) {
                    condition = false;
                }
                animals.add(new Mammals(id, splittedLine[1], splittedLine[2], Gender.parseGender(splittedLine[3]),
                        Double.parseDouble(splittedLine[4]), Double.parseDouble(splittedLine[5]), Condition.parseCondition(splittedLine[7]), condition));
            } else if (Animal.getAvesType().contains(splittedLine[1])) {
                boolean condition = true;
                if (splittedLine[6].equals("")) {
                    condition = false;
                }
                animals.add(new Aves(id, splittedLine[1], splittedLine[2], Gender.parseGender(splittedLine[3]),
                        Double.parseDouble(splittedLine[4]), Double.parseDouble(splittedLine[5]), Condition.parseCondition(splittedLine[7]), condition));
            } else {
                boolean condition = true;
                if (splittedLine[6].equals("wild")) {
                    condition = false;
                }
                animals.add(new Reptiles(id, splittedLine[1], splittedLine[2], Gender.parseGender(splittedLine[3]),
                        Double.parseDouble(splittedLine[4]), Double.parseDouble(splittedLine[5]), Condition.parseCondition(splittedLine[7]), condition));
            }
        }
        menu1();
    }

    /*
     * menu 1 berisi sections yang ada
     * */
    private static void menu1() throws IOException {
        Scanner input = new Scanner(System.in);
        int Menu = 0;
        boolean nextMenu = false;
        while (!nextMenu) {
            System.out.println("Javari Park has 3 sections:\n" +
                    "1. Explore the Mammals\n" +
                    "2. World of Aves\n" +
                    "3. Reptilian Kingdom\n");
            System.out.print("Please choose your preferred section (type the number): ");
            String selectedMenu = input.nextLine();

            if (selectedMenu.equals("1")) {
                Menu = 0;
                nextMenu = true;
            } else if (selectedMenu.equals("2")) {
                Menu = 1;
                nextMenu = true;
            } else if (selectedMenu.equals("3")) {
                Menu = 2;
                nextMenu = true;
            } else {
                System.out.println("invalid order");
            }
        }
        menu2(Menu);
    }

    /*
     * menu2 berisi jenis jenis hewan yang ada (mammals, aves, dan reptiles)
     * parameter berupa hasil input sebelumnya
     * */
    private static void menu2(int i) throws IOException {
        String selectedAnimal = "";
        ArrayList<ArrayList<String>> combinedList = new ArrayList<ArrayList<String>>();
        combinedList.add(new ArrayList<String>(Animal.getMammalType()));
        combinedList.get(0).add(0, "--Explore the Mammals--");
        combinedList.add(new ArrayList<String>(Animal.getAvesType()));
        combinedList.get(1).add(0, "--World of Aves--");
        combinedList.add(new ArrayList<String>(Animal.getReptileType()));
        combinedList.get(2).add(0, "--Reptilian Kingdom--");
        Scanner input = new Scanner(System.in);
        boolean beforeMenu = false;
        boolean nextMenu = false;
        System.out.println(combinedList.get(i).get(0));
        for (int j = 1; j < combinedList.get(i).size(); j++) {
            System.out.println(j + ". " + combinedList.get(i).get(j));
        }
        while (!nextMenu) {
            System.out.print("Please choose your preferred animals (type the number): ");
            try {
                String selectedMenu = input.nextLine();
                if (selectedMenu.equals("#")) {
                    beforeMenu = true;
                    nextMenu = true;
                } else {
                    selectedAnimal = combinedList.get(i).get(Integer.parseInt(selectedMenu));
                    nextMenu = true;
                }
            } catch (Exception e) {
                System.out.println("Invalid input");
            }

        }
        //jika menu sebelumnya yang dipilih maka kembali ke menu sebelumnya
        if (beforeMenu) {
            menu1();
        } else {
            menu3(selectedAnimal, i);
        }

    }

    /*
     * menu3 berisi jenis atraksi apa yang dapat dilakukan tipe hewan tersebut
     * */
    private static void menu3(String tipe, int i) throws IOException {
        System.out.println("---" + tipe + "---");
        System.out.println("Attractions by " + tipe + ":");
        for (int j = 0; j < Animal.attractions.get(tipe).size(); j++) {
            System.out.println((j + 1) + ". " + Animal.attractions.get(tipe).get(j));
        }
        Scanner input = new Scanner(System.in);
        String name = "";
        boolean beforeMenu = false;
        boolean nextMenu = false;
        String selectedAttraction = "";
        while (!nextMenu) {
            System.out.print("Please choose your preferred animals (type the number): ");
            try {
                String selectedMenu = input.nextLine();
                if (selectedMenu.equals("#")) {
                    beforeMenu = true;
                    nextMenu = true;
                } else {
                    selectedAttraction = Animal.attractions.get(tipe).get(Integer.parseInt(selectedMenu) - 1);
                    nextMenu = true;
                }
            } catch (Exception e) {
                System.out.println("Invalid input");
            }
        }
        if (beforeMenu) {
            menu2(i);
        } else {
            if (!regName) {
                System.out.print("Wow, one more step,\n" +
                        "please let us know your name: ");
                name = input.nextLine();
                regName = true;
                regist.setVisitorName(name);
            }
            menu4(i, selectedAttraction, tipe);
        }
    }

    /*
     * menu4 berisi validasi terakhir sebelum menggenerate json file
     * */
    private static void menu4(int i, String selectedAttractions, String tipe) throws IOException {
        Scanner input = new Scanner(System.in);
        SelectedAttractions selected = new SelectedAttractions(selectedAttractions, tipe, Animal.showableAnimal.get(tipe));
        regist.addSelectedAttraction(selected);
        System.out.println("Yeay, final check!\n" +
                "Here is your data, and the attraction you chose:\n");
        System.out.println("Name: " + regist.getVisitorName());

        for (int j = 0; j < regist.getSelectedAttractions().size(); j++) {
            System.out.println("Attractions: " + regist.getSelectedAttractions().get(j).getName() + " -> " + regist.getSelectedAttractions().get(j).getType());
            System.out.print("With: ");
            for (int k = 0; k < regist.getSelectedAttractions().get(j).getPerformers().size(); k++) {
                System.out.print(" " + regist.getSelectedAttractions().get(j).getPerformers().get(k).getName() + ",");
            }
            System.out.println();
        }
        System.out.println();
        System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
        String next = input.nextLine();
        //jika ingin kembali ke menu sebelumnya, memilih Y
        if (next.equals("Y")) {
            menu1();
        } else {
            RegistrationWriter.writeJson(regist, Paths.get(path));
        }
    }
}
