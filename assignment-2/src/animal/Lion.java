/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class dari Binatang Lion
 * */

package animal;

public class Lion extends Animal {

    //method constructor dari kelas Lion
    public Lion(String name, int size) {
        super(name, size, false);
    }

    //methods dari reaksi Lion ketika diminta user melakukan sesuatu
    public void hunt() {
        System.out.println("Lion is hunting..\n" +
                this.getName() + " makes a voice: err....");
    }

    public void brushed() {
        System.out.println("Clean the lion’s mane..\n" +
                this.getName() + " makes a voice: \nHauhhmm!");
    }

    public void disturbed() {
        System.out.println(this.getName() + " makes a voice: HAUHHMM!");
    }
}