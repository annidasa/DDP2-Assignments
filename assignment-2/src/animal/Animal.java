/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Parent class dari semua binatang
 * */
package animal;

public class Animal {

    //instance variable dari kelas animal
    private String name;
    private int size;
    //variabel untuk menentukan apakah animal tersebut pet atau wild
    private boolean isPet;

    //constructor dari class Animal
    public Animal(String name, int size, boolean isPet) {
        this.name = name;
        this.size = size;
        this.isPet = isPet;
    }

    //method untuk mendapatkan nama hewan
    public String getName() {
        return name;
    }

    //method untuk mengkonversi dari tipe animal ke Cat
    public Cat getCat() {
        return new Cat(this.name, this.size);
    }

    //method untuk mengkonversi dari tipe animal ke Eagle
    public Eagle getEagle() {
        return new Eagle(this.name, this.size);
    }

    //method untuk mengkonversi dari tipe animal ke Hamster
    public Hamster getHamster() {
        return new Hamster(this.name, this.size);
    }

    //method untuk mengkonversi dari tipe animal ke Lion
    public Lion getLion() {
        return new Lion(this.name, this.size);
    }

    //method untuk mengkonversi dari tipe animal ke Parrot
    public Parrot getParrot() {
        return new Parrot(this.name, this.size);
    }

    //method mendapat nilai sesuai nama variable
    public int getSize() {
        return size;
    }

    public boolean isPet() {
        return isPet;
    }
}