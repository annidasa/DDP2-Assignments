/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class dari Binatang Eagle
 * */
package animal;

public class Eagle extends Animal {

    //constructor kelas Eagle
    public Eagle(String name, int size) {
        super(name, size, false);
    }

    //method ketika menyuruh eagle untuk terbang
    public void fly() {
        System.out.println(this.getName() + " makes a voice: kwaakk....");
    }
}