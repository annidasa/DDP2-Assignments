/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class dari Binatang Cat
 * */
package animal;

import java.util.Random;

public class Cat extends Animal {

    //constructor dari jelas Cat
    public Cat(String name, int size) {
        super(name, size, true);
    }

    //method untuk mencetak reaksi kucing sesuai perintah yang diminta di main
    public void brushed() {
        System.out.println("Time to clean Katty's fur\n" +
                this.getName() + " makes a voice: " + "Nyaaan...");
    }

    public void cuddled() {
        Random random = new Random();
        int randomNum = random.nextInt(4);
        String voice = "";
        switch (randomNum) {
            case 0:
                voice = "Miaaaw..";
                break;
            case 1:
                voice = "Purrr..";
                break;
            case 2:
                voice = "Mwaw!";
                break;
            case 3:
                voice = "Mraaawr!";
                break;
        }
        System.out.println(this.getName() + " makes a voice: " + voice);
    }
}