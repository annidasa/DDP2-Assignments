/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class dari Binatang Parrot
 * */
package animal;

public class Parrot extends Animal {

    //constructor kelas Parrot
    public Parrot(String name, int size) {
        super(name, size, true);
    }

    //method reaksi dari Parrot ketika diminta melakukan sesuatu oleh user
    public void fly() {
        System.out.println("Parrot " + this.getName() + " flies!\n" +
                this.getName() + " makes a voice: FLYYYY…..");
    }

    public void talkBack(String words) {
        System.out.print(this.getName() + " says: ");
        if (words.equals("")) {
            System.out.println("HM?");
        } else {
            System.out.println(words.toUpperCase());
        }

    }
}