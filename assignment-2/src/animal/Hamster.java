/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class dari Binatang Hamter
 * */

package animal;

public class Hamster extends Animal {

    //constructor kelas hamster
    public Hamster(String name, int size) {
        super(name, size, true);
    }

    //method reaksi dari hamster ketika diperintahkan user di main
    public void gnaw() {
        System.out.println(this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void runInWheel() {
        System.out.println(this.getName() + " makes a voice: trrr... trrr...");
    }
}