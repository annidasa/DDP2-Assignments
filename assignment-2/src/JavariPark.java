/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Main program dari assignment-2
 * */

import animal.*;
import cages.Cages;
import cages.SortCages;

import java.util.ArrayList;
import java.util.Scanner;

public class JavariPark {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //variable untuk menentukan kapan program harus diakhiri
        boolean exit = false;

        //Arraylist dari cages berisi animal sesuai yang tertulis
        ArrayList<Cages> catCages = new ArrayList<Cages>();
        ArrayList<Cages> eagleCages = new ArrayList<Cages>();
        ArrayList<Cages> hamsterCages = new ArrayList<Cages>();
        ArrayList<Cages> lionCages = new ArrayList<Cages>();
        ArrayList<Cages> parrotCages = new ArrayList<Cages>();

        System.out.println("Welcome to Javari Park! \nInput the number of animals");

        //meminta data mengenai cat dari user serta verifikasi isinya
        System.out.print("cat: ");
        int kucing = Integer.parseInt(input.nextLine());

        if (kucing > 0) {
            boolean isInputSame = false;

            while (!isInputSame) {
                try {
                    System.out.println("Provide the information of cat(s):");
                    String[] catList = input.nextLine().split(",");
                    catCages.clear();
                    for (int i = 0; i < catList.length; i++) {
                        String[] catSplit = catList[i].split("\\|");
                        catCages.add(new Cages(new Cat(catSplit[0], Integer.parseInt(catSplit[1]))));
                    }
                    if (catCages.size() == kucing) {
                        isInputSame = true;
                    } else {
                        System.out.println("Number of given input is different with animal's data");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid input. Please re-enter your input:)");
                }

            }
        }

        //meminta data mengenai lion dari user serta verifikasi isinya
        System.out.print("lion: ");
        int singa = Integer.parseInt(input.nextLine());

        if (singa > 0) {
            boolean isInputSame = false;
            while (!isInputSame) {
                try {
                    System.out.println("Provide the information of lion(s):");
                    String[] lionList = input.nextLine().split(",");
                    lionCages.clear();
                    for (int i = 0; i < lionList.length; i++) {
                        String[] lionSplit = lionList[i].split("\\|");
                        lionCages.add(new Cages(new Lion(lionSplit[0], Integer.parseInt(lionSplit[1]))));
                    }
                    if (lionCages.size() == singa) {
                        isInputSame = true;
                    } else {
                        System.out.println("Number of given input is different with animal's data");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid input. Please re-enter your input:)");
                }
            }
        }

        //meminta data mengenai eagle dari user serta verifikasi isinya
        System.out.print("eagle: ");
        int elang = Integer.parseInt(input.nextLine());

        if (elang > 0) {
            boolean isInputSame = false;
            while (!isInputSame) {
                try {
                    System.out.println("Provide the information of eagle(s):");
                    String[] eagleList = input.nextLine().split(",");
                    eagleCages.clear();
                    for (int i = 0; i < eagleList.length; i++) {
                        String[] eagleSplit = eagleList[i].split("\\|");
                        eagleCages.add(new Cages(new Eagle(eagleSplit[0], Integer.parseInt(eagleSplit[1]))));
                    }
                    if (eagleCages.size() == elang) {
                        isInputSame = true;
                    } else {
                        System.out.println("Number of given input is different with animal's data");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid input. Please re-enter your input:)");
                }
            }
        }

        //meminta data mengenai cat dari user serta verifikasi isinya
        System.out.print("parrot: ");
        int beo = Integer.parseInt(input.nextLine());

        if (beo > 0) {
            boolean isInputSame = false;
            while (!isInputSame) {
                try {
                    System.out.println("Provide the information of parrot(s):");
                    String[] parrotList = input.nextLine().split(",");
                    parrotCages.clear();
                    for (int i = 0; i < parrotList.length; i++) {
                        String[] parrotSplit = parrotList[i].split("\\|");
                        parrotCages.add(new Cages(new Parrot(parrotSplit[0], Integer.parseInt(parrotSplit[1]))));
                    }
                    if (parrotCages.size() == beo) {
                        isInputSame = true;
                    } else {
                        System.out.println("Number of given input is different with animal's data");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid input. Please re-enter your input:)");
                }
            }
        }

        //meminta data mengenai hamster dari user serta verifikasi isinya
        System.out.print("hamster: ");
        int hamster = Integer.parseInt(input.nextLine());

        if (hamster > 0) {
            boolean isInputSame = false;
            while (!isInputSame) {
                try {
                    System.out.println("Provide the information of hamster(s):");
                    String[] hamsterList = input.nextLine().split(",");
                    hamsterCages.clear();
                    for (int i = 0; i < hamsterList.length; i++) {
                        String[] hamsterSplit = hamsterList[i].split("\\|");
                        hamsterCages.add(new Cages(new Hamster(hamsterSplit[0], Integer.parseInt(hamsterSplit[1]))));
                    }
                    if (hamsterCages.size() == hamster) {
                        isInputSame = true;
                    } else {
                        System.out.println("Number of given input is different with animal's data");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid input. Please re-enter your input:)");
                }
            }
        }

        System.out.println("Animals have been successfully recorded!\n\n=============================================");

        //menyusun ulang masing-masing hewan
        SortCages.arrange(catCages);
        SortCages.arrange(lionCages);
        SortCages.arrange(parrotCages);
        SortCages.arrange(eagleCages);
        SortCages.arrange(hamsterCages);

        System.out.println("NUMBER OF ANIMALS:" +
                "\ncat:" + kucing +
                "\nlion:" + singa +
                "\nparrot:" + beo +
                "\neagle:" + elang +
                "\nhamster:" + hamster);

        System.out.println("\n=============================================");
        //loop untuk menjalankan class
        while (!exit) {
            System.out.println("Which animal you want to visit? \n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int command = Integer.parseInt(input.nextLine());

            if (command == 1) {
                System.out.print("Mention the name of cat you want to visit: ");
                String animalName = input.nextLine();
                if (kucing == 0) {
                    System.out.print("There is no cat with that name! ");
                }
                for (int i = 0; i < kucing; i++) {
                    if (animalName.equals(catCages.get(i).getAnimal().getName())) {
                        System.out.println("You are visiting " + animalName + " (cat) now, what would you like to do?");
                        System.out.println("1: Brush the fur 2: Cuddle");
                        int command2 = Integer.parseInt(input.nextLine());

                        if (command2 == 1) {
                            catCages.get(i).getAnimal().getCat().brushed();
                        } else if (command2 == 2) {
                            catCages.get(i).getAnimal().getCat().cuddled();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        break;
                    } else if (i == catCages.size() - 1) {
                        System.out.print("There is no cat with that name! ");
                    }
                }
            } else if (command == 2) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String animalName = input.nextLine();
                if (elang == 0) {
                    System.out.print("There is no eagle with that name! ");
                }
                for (int i = 0; i < elang; i++) {
                    if (animalName.equals(eagleCages.get(i).getAnimal().getName())) {
                        System.out.println("You are visiting " + animalName + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly");
                        int command2 = Integer.parseInt(input.nextLine());

                        if (command2 == 1) {
                            eagleCages.get(i).getAnimal().getEagle().fly();
                            System.out.println("You hurt!");
                        } else {
                            System.out.println("You do nothing...");
                        }
                        break;
                    } else if (i == eagleCages.size() - 1) {
                        System.out.print("There is no eagle with that name! ");
                    }
                }
            } else if (command == 3) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String animalName = input.nextLine();
                if (hamster == 0) {
                    System.out.print("There is no hamster with that name! ");
                }
                for (int i = 0; i < hamster; i++) {
                    if (animalName.equals(hamsterCages.get(i).getAnimal().getName())) {
                        System.out.println("You are visiting " + animalName + " (hamster) now, what would you like to do?");
                        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                        int command2 = Integer.parseInt(input.nextLine());

                        if (command2 == 1) {
                            hamsterCages.get(i).getAnimal().getHamster().gnaw();
                        } else if (command2 == 2) {
                            hamsterCages.get(i).getAnimal().getHamster().runInWheel();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        break;
                    } else if (i == hamsterCages.size() - 1) {
                        System.out.print("There is no hamster with that name! ");
                    }
                }
            } else if (command == 4) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String animalName = input.nextLine();
                if (beo == 0) {
                    System.out.print("There is no parrot with that name! ");
                }
                for (int i = 0; i < beo; i++) {
                    if (animalName.equals(parrotCages.get(i).getAnimal().getName())) {
                        System.out.println("You are visiting " + animalName + " (parrot) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int command2 = Integer.parseInt(input.nextLine());

                        if (command2 == 1) {
                            parrotCages.get(i).getAnimal().getParrot().fly();
                        } else if (command2 == 2) {
                            System.out.print("You say: ");
                            String sentence = input.nextLine();
                            parrotCages.get(i).getAnimal().getParrot().talkBack(sentence);
                        } else {
                            System.out.println("You do nothing...");
                        }
                        break;
                    } else if (i == parrotCages.size() - 1) {
                        System.out.print("There is no parrot with that name! ");
                    }
                }
            } else if (command == 5) {
                System.out.print("Mention the name of lion you want to visit: ");
                String animalName = input.nextLine();
                if (elang == 0) {
                    System.out.print("There is no eagle with that name! ");
                }
                for (int i = 0; i < lionCages.size(); i++) {
                    if (animalName.equals(lionCages.get(i).getAnimal().getName())) {
                        System.out.println("You are visiting " + animalName + " (lion) now, what would you like to do?");
                        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                        int command2 = Integer.parseInt(input.nextLine());

                        if (command2 == 1) {
                            lionCages.get(i).getAnimal().getLion().hunt();
                        } else if (command2 == 2) {
                            lionCages.get(i).getAnimal().getLion().brushed();
                        } else if (command2 == 3) {
                            lionCages.get(i).getAnimal().getLion().disturbed();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        break;
                    } else if (i == singa - 1) {
                        System.out.print("There is no lion with that name! ");
                    }
                }
            } else if (command == 99) {
                exit = true;
            } else {
                System.out.println("Invalid command, please try another one\n");
            }
            System.out.println("Back to the office!\n");
        }

    }
}