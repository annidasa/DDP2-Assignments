/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class Cages yang berisi binatang-binatang
 * */

package cages;

import animal.Animal;

public class Cages {
    private Animal animal;
    private String type;
    private String cagePlace;

    //constructor kelas Cages
    public Cages(Animal animal) {
        this.animal = animal;
        this.cagesCategory();
    }

    //methods getter yang diperlukan
    public String getType() {
        return this.type;
    }

    public Animal getAnimal() {
        return this.animal;
    }

    public String getCagePlace() {
        return cagePlace;
    }

    //method untuk menggolongkan kategori Cages
    public void cagesCategory() {
        if (animal.isPet()) {
            this.cagePlace = "indoor";
            if (animal.getSize() < 45) {
                this.type = "A";
            } else if (animal.getSize() <= 60) {
                this.type = "B";
            } else {
                this.type = "C";
            }
        } else {
            this.cagePlace = "outdoor";
            if (animal.getSize() < 75) {
                this.type = "A";
            } else if (animal.getSize() <= 90) {
                this.type = "B";
            } else {
                this.type = "C";
            }
        }
    }

    //method untuk mencetak informasi dari cage yang berisi animal
    public void printCage() {
        System.out.print(" " + animal.getName() + " (" + animal.getSize() + " - " + this.type + "),");
    }
}