/*
 * Nama: Annida Safira Arief
 * NPM: 1706040050
 * Kelas: B
 * Class untuk menyusun cages of animals
 * */

package cages;

import java.util.ArrayList;


public class SortCages {
    private ArrayList<Cages> preCages2;

    //constructor kelas SortCages
    public SortCages(ArrayList<Cages> preCages) {
        this.preCages2 = preCages;
    }

    //method untuk menyusun cages
    public static void arrange(ArrayList<Cages> preCages1) {
        ArrayList<Cages> preCages = new ArrayList<Cages>(preCages1);

        //jika lebih kecil dari 0, maka program tidak akan dilanjutkan
        if (preCages.size() != 0) {
            SortCages.beforeArrange(preCages);
        }
    }

    //method yang dipanggil untuk menyusun ulang cages
    //konsep re-arrangement: level 3 dipindah ke level 1, level 1 ke level 2, dan level 2 ke level 3
    //lalu dari setiap level elemennya diswap (yang awalnya paling awal jadi paling akhir)
    private static void afterArrange(ArrayList<Cages> Level1, ArrayList<Cages> Level2, ArrayList<Cages> Level3) {
        ArrayList<Cages> temp = new ArrayList<Cages>(Level1);
        ArrayList<Cages> level1 = new ArrayList<Cages>(Level3);
        ArrayList<Cages> level3 = new ArrayList<Cages>(Level2);
        ArrayList<Cages> level2 = new ArrayList<Cages>(temp);

        temp = new ArrayList<Cages>(level1);
        level1.clear();
        for (int i = temp.size() - 1; i > -1; i--) {
            level1.add(temp.get(i));
        }

        temp = new ArrayList<Cages>(level2);
        level2.clear();
        for (int i = temp.size() - 1; i > -1; i--) {
            level2.add(temp.get(i));
        }

        temp = new ArrayList<Cages>(level3);
        level3.clear();
        for (int i = temp.size() - 1; i > -1; i--) {
            level3.add(temp.get(i));
        }

        System.out.println("After rearrangement...");
        printInfo(level1, level2, level3);
    }

    //method untuk membagi cages menjadi 3 level sesuai urutan instansiasi
    private static void beforeArrange(ArrayList<Cages> preCages) {
        ArrayList<Cages> level1 = new ArrayList<Cages>();
        ArrayList<Cages> level2 = new ArrayList<Cages>();
        ArrayList<Cages> level3 = new ArrayList<Cages>();
        if (preCages.size() < 3) {
            if (preCages.size() == 1) {
                level1.add(preCages.get(0));
            } else if (preCages.size() == 2) {
                level1.add(preCages.get(0));
                level2.add(preCages.get(1));
            }

        } else {
            if ((preCages.size() % 3 == 0) | (preCages.size() % 3 == 1)) {
                for (int i = 0; i < (preCages.size() / 3); i++) {
                    level1.add(preCages.get(i));
                }
                for (int i = (preCages.size() / 3); i < ((preCages.size() / 3)) * 2; i++) {
                    level2.add(preCages.get(i));
                }
                for (int i = ((preCages.size() / 3)) * 2; i < preCages.size(); i++) {
                    level3.add(preCages.get(i));
                }
            } else {
                for (int i = 0; i < (preCages.size() / 3); i++) {
                    level1.add(preCages.get(i));
                }
                for (int i = (preCages.size() / 3); i < ((preCages.size() / 3)) * 2 + 1; i++) {
                    level2.add(preCages.get(i));
                }
                for (int i = ((preCages.size() / 3)) * 2 + 1; i < preCages.size(); i++) {
                    level3.add(preCages.get(i));
                }
            }
        }

        System.out.println("Cage arrangement:\n" + "location: " + preCages.get(0).getCagePlace());
        printInfo(level1, level2, level3);
        System.out.println();
        afterArrange(level1, level2, level3);
        System.out.println();

    }

    private static void printInfo(ArrayList<Cages> level1, ArrayList<Cages> level2, ArrayList<Cages> level3) {
        System.out.print("level 3:");
        for (int i = 0; i < level3.size(); i++) {
            level3.get(i).printCage();
        }
        System.out.print("\nlevel 2:");
        for (int i = 0; i < level2.size(); i++) {
            level2.get(i).printCage();
        }
        System.out.print("\nlevel 1:");
        for (int i = 0; i < level1.size(); i++) {
            level1.get(i).printCage();
        }
        System.out.println();
    }
}