import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Annida Safira Arief
 * Match Board of the game
 * Some codes were inspired from https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
 * The icons are from: https://www.flaticon.com/packs/pokemon-go
 */
@SuppressWarnings("serial")
public class MatchBoard extends JFrame {

    //instance variable of the class
    private List<Cards> cards;
    private ArrayList<String> filename= new ArrayList<> (Arrays.asList("abra", "bellsprout","bulbasaur", "caterpie", "charmander",
            "dratini", "eevee", "jigglypuff", "mankey", "meowth", "mew", "pidgey", "pikachu", "psyduck", "rattata",
            "snorlax", "squirtle", "zubat", "abra", "bellsprout","bulbasaur", "caterpie", "charmander",
            "dratini", "eevee", "jigglypuff", "mankey", "meowth", "mew", "pidgey", "pikachu", "psyduck", "rattata",
            "snorlax", "squirtle", "zubat"));
    private Cards selectedCard;
    private Cards cards1;
    private Cards c2;
    private JPanel cardPanel = new JPanel();
    private JLabel attemptLabel;
    private Timer t;
    private int counter = 0;

<<<<<<< HEAD
    //ini komen baru
=======
    //omggg new comment
    //cannot waitsss
    
>>>>>>> new
    /**
     * constructor of the class
     */
    public MatchBoard() {
        Collections.shuffle(filename); //shuffle the file name
        //create the buttom
        List<Cards> cardsList = new ArrayList<Cards>();
        for (String val: filename) {
            Cards card = new Cards(val);
            card.setIcon(card.getIcon());
            card.addActionListener(ae -> {
                selectedCard = card;
                doTurn();
            });
            cardsList.add(card);

        }
        this.cards = cardsList;
        //set up the timer
        t = new Timer(750, ae -> checkCards());

        t.setRepeats(false);

        //set up the board itself
        cardPanel.setLayout(new GridLayout(6, 6));
        for (Cards c : cards) {
            cardPanel.add(c);
            c.setPreferredSize(new Dimension(100, 100));
            c.setVisible(true);
        }

        //add button to restart
        JButton playAgain = new JButton("Play Again");
        playAgain.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
                restartGame();
            }
        });
        //set the exit button
        JButton exit = new JButton("Exit");
        exit.addActionListener(ae -> System.exit(0));
        JPanel commandPanel = new JPanel();
        commandPanel.add(playAgain);
        commandPanel.add(exit);
        commandPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        attemptLabel = new JLabel("Attempts: "+counter, JLabel.CENTER);
        JPanel labelPanel = new JPanel();
        labelPanel.add(attemptLabel);

        //set the board
        setTitle("Match the Pokemon!");
        setResizable(false);
        this.add(cardPanel,BorderLayout.PAGE_START);
        this.add(commandPanel, BorderLayout.CENTER);
        this.add(attemptLabel, BorderLayout.PAGE_END);
        setPreferredSize(new Dimension(650,700)); //need to use this instead of setSize
    }

    /**
     *method to restart the game
     */
    private void restartGame() {
        cards1 = null;
        this.remove(cardPanel);
        cardPanel = new JPanel(new GridLayout(6, 6));
        Collections.shuffle(cards);
        for (Cards c : cards) {
            c.setPaired(false);
            c.hideCard();
            c.setVisible(true);
            c.setEnabled(true);
            cardPanel.add(c);
        }
        this.add(cardPanel, BorderLayout.PAGE_START);
        counter = 0;
        attemptLabel.setText("Attempts: " + counter);
    }

    /**
     * method to do turn of the card
     */
    private void doTurn() {
        if (cards1 == null && c2 == null) {
            cards1 = selectedCard;
            cards1.setIcon(cards1.showCard());
        }

        if (cards1 != null && cards1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.setIcon(c2.showCard());
            t.start();

        }
    }


    /**
     * method to check cards
     */
    private void checkCards() {
        if (cards1.getFrontString().equals(c2.getFrontString())) {
            cards1.setEnabled(false);
            c2.setEnabled(false);
            cards1.setVisible(false);
            c2.setVisible(false);
            cards1.setPaired(true);
            c2.setPaired(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!");
                for(Cards c: cards) {
                    c.setVisible(true);
                }
            }
        } else {
            cards1.setIcon(cards1.hideCard());
            c2.setIcon(c2.hideCard());
        }
        counter++;
        attemptLabel.setText("Attempts: "+counter);
        cards1 = null; //reset cards1 and c2
        c2 = null;
    }

    /**
     * method to check whether the player already won or not
     * @return boolean of whether thw player already won or not
     */
    private boolean isGameWon() {
        for (Cards c : this.cards) {
            if (!c.isPaired()) {
                return false;
            }
        }
        return true;
    }

}
