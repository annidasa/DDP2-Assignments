import java.awt.Dimension;
import javax.swing.JFrame;
import java.awt.*;

/**
 * @author annida safira
 * main program of the class
 */
public class MatchPairGame{
    public static void main(String[] args){
        MatchBoard b = new MatchBoard();
        b.setPreferredSize(new Dimension(650,700)); //need to use this instead of setSize
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }
}