import javax.swing.*;
import java.awt.Image;


public class Cards extends JButton {

    private ImageIcon icon;
    private boolean isPaired = false;
    private String frontString;

    /**
     * Constructor of Cards class
     * @param front: file name of the card
     */
    public Cards (String front) {
        this.frontString = front;
        hideCard();
    }

    /**
     * Front file name getter method
     * @return file name
     */
    public String getFrontString() {
        return this.frontString;
    }

    /**
     * is paired getter method
     * @return is the card already paired or not
     */
    public boolean isPaired() {
        return isPaired;
    }


    /**
     *method to set is the card already paired or not
     * @param paired boolean of is the card paired or not
     */
    public void setPaired(boolean paired) {
        isPaired = paired;
    }

    /**
     * return the card icon
     * @return imageicon of the card
     */
    public ImageIcon showCard() {
        formatIcon(frontString);
        return this.icon;
    }

    /**
     * return the back card icon
     * @return imageicon of the cardback
     */
    public ImageIcon hideCard() {
        String backString = "mega-ball";
        formatIcon(backString);
        return this.icon;
    }

    @Override
    public ImageIcon getIcon() {
        return icon;
    }

    /**
     * method to format the icon of the card
     * @param image string of the file name
     */
    private void formatIcon(String image) {
        this.icon = new ImageIcon(image+".png");
        Image img = this.icon.getImage() ;
        Image newimg = img.getScaledInstance( 70, 70,  java.awt.Image.SCALE_SMOOTH ) ;
        this.icon = new ImageIcon( newimg );

    }


}
